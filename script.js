console.log("script startup for Practical 4");

function pageLoaded() {

    const canvas = document.getElementById("the_canvas");
    
    const context = canvas.getContext("2d");
    
    context.fillStyle = "#0000FF";
    context.fillRect(5, 5, 100, 100);
    
    context.fillStyle = "#0000FF";
    context.fillRect(995, 5, 100, 100);
    
    context.fillStyle = "#0000FF";
    context.fillRect(5, 395, 100, 100);
    
    context.fillStyle = "#0000FF";
    context.fillRect(995, 395, 100, 100);
    
    context.beginPath();
    context.strokeStyle = "blue";
    context.moveTo(550,150);
    context.lineTo(600,175);
    context.moveTo(600,175);
    context.lineTo(625,225);
    context.moveTo(625,225);
    context.lineTo(600,275);
    context.moveTo(600,275);
    context.lineTo(550,300);
    context.moveTo(550,300);
    context.lineTo(500,275);
    context.moveTo(500,275);
    context.lineTo(475,225);
    context.moveTo(475,225);
    context.lineTo(500,175);
    context.moveTo(500,175);
    context.lineTo(550,150);
    context.stroke();

     context.beginPath();
    context.strokeStyle = "green";
    context.moveTo(255,163);
    context.lineTo(255,163);
    context.moveTo(355,103);
    context.lineTo(455,203);
    context.moveTo(455,203);
    context.lineTo(355,303);
    context.moveTo(355,303);
    context.lineTo(255,203);
    context.moveTo(255,203);
    context.lineTo(355,103);
    context.stroke();

    context.beginPath();
    context.strokeStyle = "brown";
    context.moveTo(250, 120);
    context.lineTo(100, 180);
    context.lineTo(180, 250);
    context.closePath();
    context.stroke();
    
    context.beginPath();
    context.strokeStyle = "blue";
    context.moveTo(700,100);
    context.lineTo(750,150);
    context.moveTo(750,150);
    context.lineTo(750,200);
    context.moveTo(750,200);
    context.lineTo(700,250);
    context.moveTo(700,250);
    context.lineTo(650,200);
    context.moveTo(650,200);
    context.lineTo(650,150);
    context.moveTo(650,150);
    context.lineTo(700,100);
    context.stroke();
    
    context.beginPath();
    context.strokeStyle = "yellow";
    context.moveTo(800,100);
    context.lineTo(875,100);
    context.moveTo(875,100);
    context.lineTo(900,150);
    context.moveTo(900,150);
    context.lineTo(875,200);
    context.moveTo(875,200);
    context.lineTo(800,200);
    context.moveTo(800,200);
    context.lineTo(775,150);
    context.moveTo(775,150);
    context.lineTo(800,100);
    context.stroke();

}

pageLoaded();
